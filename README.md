# JSONConfigEnhanced

NOTE: This is based off the JSONConfig created by Mark Morrill (https://github.com/AutomatonTec/JSONConfig)

This is a simple json based configuration library for Perfect.

Rather than presenting a flat json file for your configuration needs, this library lets you organize it a bit more.

## Setup:

Include the JSONConfig dependency in your project's Package.swift file:

```swift
.package(url: "https://mikesilvers@bitbucket.org/clearcodex/jsonconfigenhanced.git", from: "1.1.3")
// section dependencies
dependencies: ["JSONConfig"]
```

Rebuild your Xcode project after changing your Package.swift file.

```
swift package generate-xcodeproj
```

## Example usage:

```swift
import JSONConfigEnhanced
// somewhere, perhaps in main.swift, determine the path to your config file
#if os(Linux)
    let configSource = "./config/ApplicationConfiguration_Linux.json"
#else
    let configSource = "./config/ApplicationConfiguration_macOS.json"
#endif

// somewhere, anywhere
func setupDatabase() {
    MySQLConnector.host     = JSONConfigEnhanced.shared.string(forKeyPath: "database.host", otherwise: "127.0.0.1")
    MySQLConnector.username = JSONConfigEnhanced.shared.string(forKeyPath: "database.username", otherwise: "db_user")
    MySQLConnector.password = JSONConfigEnhanced.shared.string(forKeyPath: "database.password", otherwise: "best_password")
    MySQLConnector.database = JSONConfigEnhanced.shared.string(forKeyPath: "database.database", otherwise: "db_user")
}

func setupServer(server:HTTPServer) {
    server.serverName = JSONConfigEnhanced.shared.string(forKeyPath: "server.name", otherwise: "sub.your-domain.com")
    server.serverPort = UInt16(JSONConfigEnhanced.shared.integer(forKeyPath: "server.port", otherwise: 8080))
}
```

In your configuration json file, you can have something like:

```json
{
    "server": {
        "name": "www.your-domain.com",
        "port": 80
    },
    "database": {
        "host":     "127.0.0.1",
        "username": "db_bob",
        "password": "bob_password",
        "database": "db_bob"
    }
}
```
